package utfpr.ct.dainf.ief6ae.exemplo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

/**
 * Universidade Tecnológica Federal do Paraná 
 * IF6AE Desenvolvimento de Aplicações Wev
 * @author Alexandre Valadão Delazeri <delazeri@alunos.utfpr.edu.br>
 */

@WebServlet(name = "Sucesso Servlet", urlPatterns = {"/sucesso"})
public class SucessoServlet extends HttpServlet {
	protected void doGet (HttpServletRequest request, 
		HttpServletResponse response)
		throws ServletException, IOException {
		
		PrintWriter out = response.getWriter ();
		
		String perfil = request.getParameter("perfil");
		String nome = request.getParameter("login");

		if (perfil.equals("1")) perfil = "Cliente";
		else if (perfil.equals("2")) perfil = "Gerente";
		else perfil = "Administrador";

 		out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" "
                    + "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Sucesso</title>");
        out.println("</head>");
        out.println("<body>"); 
		out.println("<h1>Acesso a " + perfil + " " + nome + " permitido</h1>");
		out.println("</body>");
		out.println("</html>");
		}
}
