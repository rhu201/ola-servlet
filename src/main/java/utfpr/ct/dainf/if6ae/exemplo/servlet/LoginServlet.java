package utfpr.ct.dainf.if6ae.exemplo.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

/**
 * Universidade Tecnológica Federal do Paraná 
 * IF6AE Desenvolvimento de Aplicações Wev
 * @author Alexandre Valadão Delazeri <delazeri@alunos.utfpr.edu.br>
 */

@WebServlet(name = "Login Servlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
	protected void doPost (HttpServletRequest request, 
		HttpServletResponse response) 
		throws ServletException, IOException {
		
		String nome = request.getParameter("login");
		String senha = request.getParameter("senha");
		String perfil = request.getParameter("perfil");
		if (nome.equals(senha)) {
			
			response.sendRedirect ("sucesso?login="+nome+"&perfil="+perfil);
		} else {
			response.sendRedirect ("erro.xhtml");
		}
	}
}
